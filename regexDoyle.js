'use strict';
const $ = function (foo) { return document.getElementById(foo); };
const ajaxobj = new XMLHttpRequest();

const getFile = function (ajax, url, callback) {
    console.log(`nml: ${url}`);
    try {
        ajax.addEventListener('load', function(ev) {    
            callback(ev);
        });
        ajax.open('get', url);
        ajax.send('');
    } catch(err) {
        window.alert(`WTF: \n${err.message}`);
    } 
}

const handler = function (e) {
    $('remote').innerHTML = '';
    let s = e.target.responseText;
    //console.log('nml s ' + s);
    
    //let r = /<a\s*href=[\'"](.+?)[\'"].*?>/gi;
    let r = /<a\s*href=[\'"](.+?)[\'"].*?>(.*)<\/a>/gi; //Modified to get text also.

    let t = /<a\s.*>(.*)<\/a>/; //Get the text between <a> and </a> tags
    let a = s.match(r);
    //console.log(`\nml ${a.length} ${a[1].length}\n ${a}`);
    
    let di = document.createElement('div');
    let h2 = document.createElement('h2');
    let h2t = document.createTextNode(`Linked URLs found at ${url}`);
    h2.appendChild(h2t);
    di.appendChild(h2);
    let ul = document.createElement('ul');
    for (let elm of a.slice(1)) {
        let li = document.createElement('li');
        let lit = document.createTextNode(elm);
        li.appendChild(lit);
        ul.appendChild(li);
    }
    di.appendChild(ul);
    $('remote').appendChild(di);
}

const getRemoteContent = function (e) {
    let url = $('url').value;
    e.preventDefault();
    getFile(ajaxobj, url, handler);
}

const showStarter = function () {
    const titles = document.getElementsByClassName('title');
    for (let title of titles)
        title.innerHTML = 'Get Linked URLs from Webpage';
    $('btn').addEventListener('click', getRemoteContent);
}

window.addEventListener("load", showStarter);                   // kick off JS