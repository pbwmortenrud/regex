const $ = function (foo) {
  return document.getElementById(foo);
};
//Test if string is found in the basestring (true)
function test1() {
  const string = "all your base are belong to us";
  const regex = /base/;
  const isExisting = regex.test(string);

  console.log(isExisting);
}
//Find a name in a string, and print that.
function test2() {
  const string = "My name is Morten";
  const regex = /name is ([a-zA-Z]+)/;
  const match = regex.exec(string);
  if (match) {
    const name = match[1];
    console.log(`Your name is ${name}`);
  } else console.log("no match");
}
//Find multiple groups/variables in a regex tested string.
function test3() {
  const string = "filea.mp3 file_01.mp3 text.csv other.txt";
  const regex = /(\w+)\.mp3/g; // g is for global. If omitted, the regEx only returns the first occurrance.
  let match = regex.exec(string);
  while (match) {
    const filename = match[1];
    console.log(filename);
    match = regex.exec(string);
  }
}
//look for a number in a string. If 666 then break;
function test4() {
  const numero = function (s) {
    let r = /(\d+)/;
    let a = s.match(r);
    return Number(a[1]);
  };

  while (true) {
    let s = prompt("enter string with a number");
    let n = numero(s);
    console.log(n);
    if (n === 666) break;
  }
}

const linkA = "http://www.example.com/";
const linkB = "http://www.example.com/hello/there.html";
const linkC = "http://example.com/hello/there.html";
const linkD = "www.example.com/hello/there.html";
const linkE = "https://www.example.com";

//Assignment JS.Regex.0 (http://dkexit.eu/webdev/site/ch31s04.html#idm6602)
function findDomainName(string) {
  let domainName = "not found";
  const regex = /([w]{3}\.)?(\w+\.\w+)\/?/; //Group 1 = 'www.', optional. Group 2 = 'word.word/', whereas '/' is optional

  const match = regex.exec(string);
  if (match) {
    domainName = match[2];
    console.log(`domain name of ${string} was \n :${domainName}`);
  } else console.log("no match");
  return domainName;
}
//test it
findDomainName(linkA);
findDomainName(linkB);
findDomainName(linkC);
findDomainName(linkD);
findDomainName(linkE);

//Assignment JS.Regex.1
const links = { linkA, linkB, linkC, linkD, linkE };
const names = {
  linkA: "Alpha",
  linkB: "Bravo",
  linkC: "Charlie",
  linkD: "Delta",
  linkE: "Echo",
};
console.log(links);
function drawLinks() {
  for (const key in links) {
    let a = document.createElement("a");
    a.href = links[key];
    a.innerText = names[key];

    document.getElementById("links").appendChild(a);
    document.getElementById("links").appendChild(document.createElement("br"));
    //document.getElementById('links-txt').innerText = a;

    console.log(a);
  }
}
drawLinks();
function findAtag(string) {
  let result = "not found";
  const regex = /<a\s*href=[\'"](.+?)[\'"].*?>(.*?)<\/a>/gi; //Modified to get text also. (https://regex101.com/r/wXhjIe/1/)
  let match = regex.exec(string);
  let textArr = [];
  while (match) {
    console.log("MATCH " + match[0]);
    const href = match[1];
    const text = match[2];
    result = `Href: ${href}, Text: ${text} \n `;
    console.log(result);
    textArr.push(result);
    match = regex.exec(string);
  }
  textArr.forEach((element) => {
    document.getElementById("links-txt").innerText += element;
  });
}
findAtag(document.getElementById("links").innerHTML);

/*
let r = /<a\s*href=[\'"](.+?)[\'"].*?>/gi;
let t = /<a\s.*>(.*)<\/a>/; //Get the text between <a> and </a> tags
r = /<a\s*href=[\'"](.+?)[\'"].*?>(.*?)<\/a>/gi; //Modified to get text also. (https://regex101.com/r/fEaLKD/1/)
*/

//Assignment JS.Regex.3
//https://regex101.com/r/FX4Kjf/1/
//https://regex101.com/r/QCHYXn/1 - WITH WHOLE POEM
function findWords(string) {
  let wordList = [];
  const regex = /(\b.+?[-\w]?)[\s,.!"]+/gi;
  match = regex.exec(string);
  while (match) {
    let word = match[1] + ""; //force string;
    wordList.push(word.toLowerCase());
    match = regex.exec(string);
  }
  return wordList;
}
const testStr = `The secret of a poem, no less than a jest's prosperity, lies in the ear of
him that hears it. Yield to its spell, accept the poet's mood: this, after
all, is what the sages answer when you ask them of its value. Even though
the poet himself, in his other mood, tell you that his art is but sleight
of hand, his food enchanter's food, and offer to show you the trick of
it,--believe him not. Wait for his prophetic hour; then give yourself to
his passion, his joy or pain. "We are in Love's hand to-day!" sings
Gautier, in Swinburne's buoyant paraphrase,--and from morn to sunset we are
wafted on the violent sea: there is but one love, one May, one flowery
strand. Love is eternal, all else unreal and put aside. The vision has an
end, the scene changes; but we have gained something, the memory of a
charm. As many poets, so many charms. There is the charm of Evanescence,
that which lends to supreme beauty and grace an aureole of Pathos. Share
with Landor his one "night of memories and of sighs" for Rose Aylmer, and
you have this to the full.`;

function uniqueWordList(wordList = []) {
  let uniqeWordList = { "": "" };
  uniq = [...new Set(wordList)]; //a Set can only contain unique values. "..." is a spread syntax which in this context is equal to "foreach value in Set(wordList)".
  return uniq;
}

function separateWordsAndCount(wordList) {
  let wordCount = {};

  for (let i = 0; i < wordList.length; i++) {
    const element = wordList[i];
    if (!wordCount[element]) wordCount[element] = 0;
    ++wordCount[element];
  }
  return wordCount;
}

function countUniqueWords(wordList) {
    let newList = new Map();
    for (let i = 0; i < wordList.length; i++) {
        const element = wordList[i];
        
        if (!newList[element]) {
            newList[element] = 1;
        }
        else newList[element]++;
    }
    console.log(newList);
    // let sortedList = sortMap(newList);
    // console.log(sortedList);
}


//DO CODE
//TEST
console.log(uniqueWordList(findWords(text2))); //Return 2273 before lowerCase. Return 2104 after lowerCase

//Draw on document:

let poemWords = findWords(text2);
let uniqPoemWords = uniqueWordList(poemWords);

countUniqueWords(poemWords);
// console.log(separateWordsAndCount(poemWords));
// let wordCounts = separateWordsAndCount(poemWords);
// console.log(wordCounts);

$("words").innerHTML += `<h3>Words in poem: ${poemWords.length}</h3>`;
$(
  "words"
).innerHTML += `<h3>Unique words in poem: ${uniqPoemWords.length}</h3>`;

